FROM node:12.0-slim
WORKDIR /app
COPY . .
ENV PATH /app/node_modules/.bin:$PATH
COPY ["package.json", "package-lock.json*", "./"]
COPY public /app/public
COPY kube /app/kube
COPY views /app/views
RUN npm install
CMD [ "node", "index.js" ]